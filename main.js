// Depends on tencentcloud-sdk-nodejs version 4.0.3 or higher

const tencentcloud = require("tencentcloud-sdk-nodejs");

const TmsClient = tencentcloud.tms.v20201229.Client;

const clientConfig = {
    credential: {
        secretId: "secretId",
        secretKey: "secretKey",
    },
    region: "ap-shanghai",
    profile: {
        httpProfile: {
            endpoint: "tms.tencentcloudapi.com",
        },
    },
};
const client = new TmsClient(clientConfig);
const glob = require("glob");
const fs = require('fs')
// const stats = fs.stat()
var getDirectories = function (src, callback) {
    glob(src + '/**/*.rb', callback);
};
getDirectories('controllers', function (err, res) {
    if (err) {
        console.log('Error', err);
    }
    console.log('files:' + res.length)

    const contents = res.map((pathStr) => {
        let content = fs.readFileSync(pathStr)
        return Buffer.from(content).toString('base64')
    })

    const tasks = contents.map(content => {
        let p = new Promise((resolve, reject) => {
            const params = {
                Content: content,
            };
            client.TextModeration(params).then(
                (data) => {
                    resolve(data)
                },
                (err) => {
                    console.log(err)
                    resolve(err)
                }
            );
        })
        return p
    })
    const start = new Date()
    Promise.all(tasks).then(() => {
        console.log('time：' + (new Date() - start))
    })
});




